################################################################################
#                      Core TISDK configuration file                           #
################################################################################

# This variable contains the list of variables found in this config file
# that MUST be set to a value
MANDATORY_INPUTS="MACHINES BUILD_ID SCRIPTS_ROOT BUILD_ROOT DEPLOY_ROOT CONFIG_FILE LOG_DIR TISDK_VERSION"

# This variable holds the SDK version you are building
TISDK_VERSION="live"

# This variable contains the machine name to build for
MACHINES=""

# SECDEV variables for CAT, AUTO and K3
TI_SECURE_DEV_PKG_CAT="/opt/secdev-cat"
TI_SECURE_DEV_PKG_AUTO="/opt/secdev-auto"
TI_SECURE_DEV_PKG_K3="/opt/secdev-k3"

# This variable holds the unique ID for the build that will be used to
# identify the build.
BUILD_ID="`date +build-%F_%H-%M-%S`"

# This variable contains the script directory where these scripts are running
# from.  This is to allow finding other files used during the build later.
# You should likely not change this
SCRIPTS_ROOT="$PWD"

# This variable points to be base of your build where the sources will be
# checked out and the tmp build diretories for each architecture will be
# created using the oe-layertool-setup.sh script from the repo at:
#   git clone git://arago-project.org/git/projects/oe-layersetup.git
BUILD_ROOT="$SCRIPTS_ROOT/farm-arago-dunfell-next-build"

# This variable points to the base location where the SDK files will
# be deployed.  This is essentially the final staging directory
DEPLOY_ROOT="$BUILD_ROOT/deploy/$TISDK_VERSION"

# This variable points to the location where the sources will be checked out
# using the oe-layertool-setup.sh script from the following repository:
#   git clone git://arago-project.org/git/projects/oe-layersetup.git
SOURCE_DIR="${BUILD_ROOT}/sources"

# This variable indicates whether we should do a clean build which means
# Removing the build directories
CLEAN_BUILD="true"

# This variable indicates whether we should clean shared state (sstate)
CLEAN_SSTATE="true"

# This variable means that we should clean not only build directories,
# but also the sources as well and do a fully clean build.
CLEAN_ALL="false"

# This is the configuration file to use in the oe-layertool-setup.sh script
# to configure the build.
CONFIG_FILE="${BUILD_ROOT}/configs/arago-dunfell-next-config.txt"

# This directory will contain the log files for the various parts of the build.
LOG_DIR="${BUILD_ROOT}/logs"

# Setting this variable to "true" will enable logging stdout and stderr of the
# bitbake command to the appropriate machine's build_log.txt.
BUILD_LOG_VERBOSE="true"

# This is the directory where you want to configure your build to download
# files to.  This is best done as a shared directory among the builds to
# save download time and space.
DL_DIR="${BUILD_ROOT}/downloads"

# Setting this variable will configure the builds to attempt to use a mirror to 
# obtain the build sources.
SOURCE_MIRROR_URL="http://lcpd.itg.ti.com/sources/"

# Seeting this variable to "1" configures the build to generate mirror tarballs
# of the source repos.
BB_GENERATE_MIRROR_TARBALLS="1"

# This variable contains the command(s) that bitbake should run. Each command
# should be seperated by a semicolon.
BITBAKE_COMMAND="-k tisdk-core-bundle tisdk-tiny-image ti-world"
