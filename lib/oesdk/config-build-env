#!/bin/bash

TI_SDK_OELAYER_SETUP="git://git.ti.com/arago-project/oe-layersetup.git"

# This script is intended to be called only from the top-level build-oesdk.sh
# script which parses the configuration file and sets necessary variables.
# So check if it is being called from any other context and if so exit.
if [ "`basename $0`" != "build-oesdk.sh" ]
then
    echo "This is a helper script and should not be run directly"
    exit 1
fi

# Checkout the layer scripts into our build root.  In this case the following
# conditions apply:
#   1. The checkout creates the final directory of BUILD_ROOT
#   2. If a git repo already exists then do a git pull
#   3. If the directory is not empty and is not a git repo, then cleanup if unused
checkout_layer_scripts() {
    cd `dirname $BUILD_ROOT`

    name=`basename $BUILD_ROOT`

    # Paranoid check: Am I really a build folder?
    amibuild=`echo $BUILD_ROOT|grep "build$"`
    if [ -z "$amibuild" ]; then
        echo "Something horrible has happened.. BUILD_ROOT=$BUILD_ROOT does'nt end with 'build'. maynot be build folder. Aborting!"
        exit 1
    fi

    # If the scripts folder exists and is not a git repo, cleanup.
    if [ -e $name -a ! -d $name/.git ]; then
        echo "The $BUILD_ROOT directory is present and is not a git repo - attempting to wipe it off"
        users=`lsof "$BUILD_ROOT"|wc -l`
        if [ $users -gt 0 ]; then
           echo "ERROR:: We have users for $BUILD_ROOT folder: Aborting!!!"
           lsof $BUILD_ROOT
           pstree -aplG
           exit 1
        fi
        rm -rvf "$BUILD_ROOT" || exit 1
    fi


    if [ ! -d $name ]; then
        git clone "$TI_SDK_OELAYER_SETUP" $name
    else
        # This looks to be a git repo.  Hopefully it is the right one :)
        cd $BUILD_ROOT
        # In case we have changed the location of our build scripts
        # we should change our git url towards it.
        mygit='git remote get-url  origin'
        if [ "$mygit" != "$TI_SDK_OELAYER_SETUP" ]; then
            echo "$TI_SDK_OELAYER_SETUP Mismatch in git URL: $mygit vs $TI_SDK_OELAYER_SETUP. Force resetting the origin!"
            git remote set-url origin $TI_SDK_OELAYER_SETUP
        fi

        git fetch
        git reset --hard origin
    fi
}

# This function will create the build directory a given build list with
# the following features:
#   1. Modify the conf/local.conf to point to the following:
#       - DL_DIR = DL_DIR
#       - SSTATE_DIR = BUILD_ROOT/sstate-cache
#       - TMPDIR = BUILD_ROOT/<build dir>/arago-tmp
#       - DEPLOY_DIR = BUILD_ROOT/<build dir>/deploy
#   2. Modify the conf/setenv file to point to the following:
#       - BUILDDIR=BUILD_ROOT/<build dir>
config_build_dir() {
    ./oe-layertool-setup.sh -r -f ${CONFIG_FILE}
    build_dir="build"

    # Change directory into build_dir/conf to make life easier here.
    # Change back when done.
    cd $build_dir/conf
    sed -i "s+^DL_DIR =.*+DL_DIR = \"$DL_DIR\"+" local.conf
    sed -i "s+^SSTATE_DIR =.*+STATE_DIR = \"$BUILD_ROOT/sstate-cache\"+" local.conf
    sed -i "s+^TMPDIR =.*+TMPDIR = \"$BUILD_ROOT/$build_dir/arago-tmp\"+" local.conf
    sed -i "s+^DEPLOY_DIR =.*+DEPLOY_DIR = \"$BUILD_ROOT/$build_dir/deploy\"+" local.conf
    sed -i "s+^export BUILDDIR=.*+export BUILDDIR=\"$BUILD_ROOT/$build_dir\"+" setenv

    if [ ! -z "$SOURCE_MIRROR_URL" ]; then
      echo "SOURCE_MIRROR_URL = \"$SOURCE_MIRROR_URL\"" >> local.conf
      echo "INHERIT += \"own-mirrors\"" >> local.conf
    fi

    if [ "$EXPORT_ARAGO_SRC" == "1" ]; then
      BB_GENERATE_MIRROR_TARBALLS=1
    fi

    if [ ! -z "$BB_GENERATE_MIRROR_TARBALLS" ]; then
      sed -i "s+^BB_GENERATE_MIRROR_TARBALLS =.*+BB_GENERATE_MIRROR_TARBALLS = \"$BB_GENERATE_MIRROR_TARBALLS\"+" local.conf
    fi

    if [ ! -z "$PRSERV_HOST" ]; then
      sed -i "s+^PRSERV_HOST =.*+PRSERV_HOST = \"$PRSERV_HOST\"+" local.conf
    fi

    if [ ! -z "$SSTATE_MIRRORS" ]; then
      echo "SSTATE_MIRRORS = \"$SSTATE_MIRRORS\"" >> local.conf
    fi

    if [ ! -z "$LOCAL_CONF_APPEND" ]; then
      cat "$LOCAL_CONF_APPEND" >> local.conf
    fi

    if [ ! -z "$LOCAL_CONF_ADD_VAR" ]; then
      echo "$LOCAL_CONF_ADD_VAR" >> local.conf
    fi
    cd - > /dev/null 2>&1
}
