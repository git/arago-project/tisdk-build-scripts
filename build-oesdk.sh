#!/bin/bash

# Setup signal traps to stop any running background processes as well in
# the case that the script is stopped or exits
trap 'test -z "`jobs -p`" || kill `jobs -p` > /dev/null 2>&1; echo "I got a signal"' SIGINT SIGQUIT SIGTSTP EXIT

# Source scripts containing the helper functions used in this top-level
# script.
source ./lib/oesdk/config-build-env
source ./lib/common-functions
source ./lib/oesdk/bitbake-test-sdk-image

if [ -z $LOG_DIR ]
then
    LOG_DIR="."
fi

usage() {
    echo -n "Usage: "
    echo -n `basename $0`
    echo " [-h] [-s] [-m machine] -f config"
    echo "   -f config    Config file to use for the build"
    echo "   -m machine   Build for machine instead of what set in config"
    echo "   -s           Skip all repositories checkouts"
    echo "   -h           This help screen"
    exit 0
}

###############
# Main Script #
###############

# Parse input options
while getopts :f:m:hs arg
do
    case $arg in
        f ) inputfile="$OPTARG";;
        m ) machine="$OPTARG";;
        s ) skipcheckout="true";;
        h ) usage;;
    esac
done

# Do not allow calling this script without passing the config file option
if [ "$inputfile" = "" ]
then
    log $error_log echo "You must specify a config file to use"
    log $error_log usage
    exit 1
fi

# Parse the input config file to set the required build variables
parse_config_file $inputfile

if [ "$machine" != "" ]
then
    echo "Machine $machine was specified as a parameter on the command line"
    MACHINES=$machine
fi

# Verify that the MANDATORY_INPUTS have been set.  We know there are some
# values that absolutely need to be set or else you will end up
# trashing your host configuration.
if [ "$MANDATORY_INPUTS" == "" ]
then
    echo "Mandatory inputs not set"
    exit 1
fi

check_mandatory_inputs $MANDATORY_INPUTS

if [ `echo $MACHINES | wc -w` != 1 ]
then
    echo "Only a single machine must be specified"
    exit 1
fi

# Clean the build if the CLEAN Flags are set
if [ "$CLEAN_ALL" == "true" ]
then
    echo "Removing $BUILD_ROOT"
    rm -rf $BUILD_ROOT
else
    if [ "$CLEAN_BUILD" == "true" ]
    then
        echo "Removing $BUILD_ROOT/build/arago-tmp-*"
        rm -rf $BUILD_ROOT/build/arago-tmp-*
    fi
    if [ "$CLEAN_SSTATE" == "true" ]
    then
        echo "Removing $BUILD_ROOT/build/sstate-cache"
        rm -rf $BUILD_ROOT/build/sstate-cache
        rm -rf $BUILD_ROOT/build/buildhistory
    fi
fi

if [ -f "$BUILD_ROOT/build/bitbake.lock" ]; then
    echo "Removing $BUILD_ROOT/build/bitbake.lock"
    rm -rf $BUILD_ROOT/build/bitbake.lock
fi

# Make the directory structure leading up to BUILD_ROOT.  BUILD_ROOT will
# be created when the setup scripts are cloned.
mkdir -p `dirname $BUILD_ROOT`

# Checkout the scripts to configure the build layers
if [ "$skipcheckout" == "true" ]
then
    echo "Skipping checkout of oe-layersetup scripts"
else
    checkout_layer_scripts
fi

# Create the LOG_DIR
mkdir -p $LOG_DIR

# Clean the old log files.
rm -rf $LOG_DIR/*

# The rest of this script will assume we are running from BUILD_ROOT
cd $BUILD_ROOT

log $general_log config_build_dir

bitbake_sdk_images

echo "Build Finished"
